<?php

use \App\HttpController\Router;

//管理后台
Router::group(['prefix'=>'count','namespace'=>'Admin/IndexController'],function (){
    //按钮列表
    Router::get('btn/list', 'btnList');
    Router::post('btn/update', 'updateList');
    //按钮统计
    Router::get('btn', 'btn');
    //formid统计
    Router::get('form', 'form');
    //分享回流统计
    Router::get('share', 'share');
    //模板消息回流统计
    Router::get('tpl', 'tpl');
    //访问统计
    Router::get('visit','visit');
});
