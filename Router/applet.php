<?php

use \App\HttpController\Router;

//前台
Router::group(['prefix'=>'api','namespace'=>'Applet/IndexController'],function (){
    //按钮统计
    Router::post('btn', 'btn');
    //formid统计
    Router::post('form', 'form');
    //分享回流统计
    Router::post('share', 'share');
    //模板消息回流统计
    Router::post('tpl', 'tpl');
    //访问统计
    Router::post('visit','visit');
});
