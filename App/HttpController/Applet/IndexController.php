<?php

namespace App\HttpController\Applet;

use App\HttpController\BaseController;
use App\Utility\RedisClient;

class IndexController extends BaseController {
    //缓存按小程序进行分组
    protected $date = '';//日期
    protected $hour = '';//小时
    protected $pages = ['index','info'];

    public function __construct(){
        $this->date = date('Y-m-d',time());
        $this->hour = date('H',time());
        parent::__construct();
    }

    public function form(){
        $request = $this->request();
        $applet= trim($request->getRequestParam('applet')) ?: '';
        $openid= trim($request->getRequestParam('openid')) ?: '';
        $page  = trim($request->getRequestParam('page')) ?: '';
        if(empty($applet) || empty($openid) || empty($page) || !in_array($page,$this->pages)){
            return $this->writeJson(1,null,'参数错误');
        }
        //写入队列
        $redisClient = new RedisClient();
        $queue = [
            'applet' => $applet,
            'openid' => $openid,
            'page' => $page,
            'date' => $this->date,
            'hour' => $this->hour,
        ];
        $redisClient->onQueue('form')->push($queue);
        unset($redisClient);
        return $this->writeJson(0);
    }

    public function share(){
        $request = $this->request();
        $applet= trim($request->getRequestParam('applet')) ?: '';
        $openid= trim($request->getRequestParam('openid')) ?: '';
        if(empty($applet) || empty($openid)){
            return $this->writeJson(1,null,'参数错误');
        }
        //写入队列
        $redisClient = new RedisClient();
        $queue = [
            'applet' => $applet,
            'openid' => $openid,
            'date' => $this->date,
            'hour' => $this->hour,
        ];
        $redisClient->onQueue('share')->push($queue);
        unset($redisClient);
        return $this->writeJson(0);
    }

    public function visit(){
        $request = $this->request();
        $applet= trim($request->getRequestParam('applet')) ?: '';
        $openid= trim($request->getRequestParam('openid')) ?: '';
        $out   = trim($request->getRequestParam('out')) ?: '';
        $page  = trim($request->getRequestParam('page')) ?: '';
        if(empty($applet) || empty($openid) || empty($page) || !in_array($page,$this->pages)){
            return $this->writeJson(1,null,'参数错误');
        }
        //写入队列
        $redisClient = new RedisClient();
        $queue = [
            'applet' => $applet,
            'openid' => $openid,
            'out'    => $out,
            'page'   => $page,
            'date'   => $this->date,
            'hour'   => $this->hour,
        ];
        $redisClient->onQueue('visit')->push($queue);
        unset($redisClient);
        return $this->writeJson(0);
    }

    public function tpl(){
        $request = $this->request();
        $applet= trim($request->getRequestParam('applet')) ?: '';
        $openid= trim($request->getRequestParam('openid')) ?: '';
        if(empty($applet) || empty($openid)){
            return $this->writeJson(1,null,'参数错误');
        }
        //写入队列
        $redisClient = new RedisClient();
        $queue = [
            'applet' => $applet,
            'openid' => $openid,
            'date' => $this->date,
            'hour' => $this->hour,
        ];
        $redisClient->onQueue('tpl')->push($queue);
        unset($redisClient);
        return $this->writeJson(0);
    }

    public function btn(){
        $request = $this->request();
        $applet= trim($request->getRequestParam('applet')) ?: '';
        $openid= trim($request->getRequestParam('openid')) ?: '';
        $name  = trim($request->getRequestParam('name')) ?: '';
        $page  = trim($request->getRequestParam('page')) ?: '';
        if(empty($applet) || empty($name) || empty($openid) || empty($page) || !in_array($page,$this->pages)){
            return $this->writeJson(1,null,'参数错误');
        }
        //写入队列
        $redisClient = new RedisClient();
        $queue = [
            'applet' => $applet,
            'openid' => $openid,
            'name'   => $name,
            'page'   => $page,
            'date'   => $this->date,
            'hour'   => $this->hour,
        ];
        $redisClient->onQueue('button')->push($queue);
        unset($redisClient);
        return $this->writeJson(0);
    }


}
