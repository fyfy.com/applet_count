<?php

namespace App\HttpController;

use EasySwoole\Http\AbstractInterface\Controller;

abstract class BaseController extends Controller{

    public function index() {
        $this->actionNotFound('index');
    }

    /**
     * 重置writeJson 方法
     * @param int $statusCode 0成功 1失败
     * @param null $result 结果
     * @param null $msg 消息提示
     * @return bool
     */
    protected function writeJson($statusCode = 0,$result = null,$msg = null){
        if(!$this->response()->isEndResponse()){
            $data = [
                "code"  => $statusCode,
                "data"  => $result,
                "msg"   => $msg ?? 'SUCCESS'
            ];
            $this->response()->write(json_encode($data,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
            $this->response()->withHeader('Content-type','application/json;charset=utf-8');
            $this->response()->withStatus(200);
            return true;
        }else{
            return false;
        }
    }

    /**
     * 获取IP
     * @return string
     */
    protected function getIp(){
        $ip = $this->request()->getHeaders();
        return $ip['x-real-ip'][0] ?: '0.0.0.0';
    }

    /**
     * 状态转换
     * @param $data
     * @param array $map
     * @return mixed
     */
    protected function stateToText(&$data, $map = []){
        foreach ($data as $key => &$row) {
            foreach ($map as $col => $pair) {
                if (isset($row[$col]) && isset($pair[$row[$col]])) {
                    $text = $col . '_text';
                    $row[$text] = $pair[$row[$col]];
                }
            }
        }
        return $data;
    }

    /**
     * 把返回的数据集转换成Tree
     * @param array $list 要转换的数据集
     * @param string $pid parent标记字段
     * @param string $level level标记字段
     * @return array
     */
    public function listToTree($list, $pk='id', $pid='pid', $child='children', $root=0){
        // 创建Tree
        $tree = [];
        // 创建基于主键的数组引用
        $refer = [];
        foreach ($list as $key => $data) {
            $refer[$data[$pk]] =& $list[$key];
        }
        foreach ($list as $key => $data) {
            // 判断是否存在parent
            $parentId = $data[$pid];
            if ($root == $parentId) {
                $tree[] =& $list[$key];
            } else {
                if (isset($refer[$parentId])) {
                    $parent =& $refer[$parentId];
                    $parent[$child][] =& $list[$key];
                }
            }
        }
        return $tree;
    }


}
