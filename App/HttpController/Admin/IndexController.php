<?php

namespace App\HttpController\Admin;

use App\HttpController\BaseController;
use App\Model\ButtonsModel;
use App\Model\CountButtonsModel;
use App\Model\CountFormidsModel;
use App\Model\CountSharesModel;
use App\Model\CountTplsModel;
use App\Model\CountVisitesModel;
use EasySwoole\EasySwoole\Logger;

class IndexController extends BaseController {

    //获取按钮列表
    public function btnList(){
        $btnListModel = new ButtonsModel();
        $list = $btnListModel->get();
        return $this->writeJson(0,$list);
    }
    //更新按钮
    public function updateList(){
        $request = $this->request();
        $params = $request->getRequestParam();
        if(!isset($params['name']) || !$params['name']){
            return $this->writeJson(1,null,'name is nll');
        }
        $btnListModel = new ButtonsModel();
        if(isset($params['id']) && $params['id']){
            $info = $btnListModel->where('name',$params['name'])->where('id',$params['id'],'<>')->first();
            if($info){
                return $this->writeJson(1,null,'name已经存在');
            }
            $btnListModel->update($params);
        }else{
            //新增
            $info = $btnListModel->where('name',$params['name'])->first();
            if($info){
                return $this->writeJson(1,null,'name已经存在');
            }
            $btnListModel->create($params);
        }
        return $this->writeJson(0);
    }
    //按钮统计
    public function btn(){
        $request = $this->request();
        $params = $request->getRequestParam();
        if(!$params['applet']){
            return $this->writeJson(0,null,'error');
        }
        $select = 'applet,count_buttons.name,SUM(total_nums) as total_nums,SUM(user_nums) as user_nums, SUM(touch_nums) as touch_nums,page,created_at,buttons.title';
        $btnModel = new CountButtonsModel();
        $list = $btnModel
            ->select($select)
            ->where(function ($query) use ($params){
                $query->where('applet',$params['applet']);
                if($params['date']){
                    $query->where('created_at',$params['date']);
                }else{
                    $query->where('created_at',date('Y-m-d',time()));
                }
                if($params['name']){
                    $query->where('name',$params['name']);
                }
                if($params['page']){
                    $query->where('page',$params['page']);
                }
            })
            ->join('buttons','count_buttons.name = buttons.name','RIGHT')
            ->groupBy('name')
            ->get();
        return $this->writeJson(0,$list);
    }
    //formid统计
    public function form(){
        $request = $this->request();
        $params = $request->getRequestParam();
        if(!$params['applet']){
            return $this->writeJson(0,null,'error');
        }
        $btnModel = new CountFormidsModel();
        $list = $btnModel
            ->where(function ($query) use ($params){
                $query->where('applet',$params['applet']);
                if($params['date']){
                    $query->where('created_at',$params['date']);
                }else{
                    $query->where('created_at',date('Y-m-d',time()));
                }
                if($params['page']){
                    $query->where('page',$params['page']);
                }
            })
            ->get();
        $total_nums = array_column($list,'total_nums','created_hour');
        $user_nums = array_column($list,'user_nums','created_hour');
        $touch_nums = array_column($list,'touch_nums','created_hour');
        $return[] = $this->getAnalyArray('点击总次数',$total_nums);
        $return[] = $this->getAnalyArray('日活用户数',$user_nums);
        $return[] = $this->getAnalyArray('触发用户数',$touch_nums);
        //统计和
        if($params['date']){
            $count['date'] = $params['date'];
        }else{
            $count['date'] = date('Y-m-d',time());
        }
        $count['total_nums'] = array_sum($total_nums);
        $count['user_nums'] = array_sum($user_nums);
        $count['touch_nums'] = array_sum($touch_nums);

        return $this->writeJson(0,['list' =>$return,'count'=>$count]);
    }
    //分享回流统计
    public function share(){
        $request = $this->request();
        $params = $request->getRequestParam();
        if(!$params['applet']){
            return $this->writeJson(0,null,'error');
        }
        $btnModel = new CountSharesModel();
        $list = $btnModel->where(function ($query) use ($params){
            $query->where('applet',$params['applet']);
            if($params['date']){
                $query->where('created_at',$params['date']);
            }else{
                $query->where('created_at',date('Y-m-d',time()));
            }
        })->get();
        $total_nums = array_column($list,'total_nums','created_hour');
        $user_nums = array_column($list,'user_nums','created_hour');
        $touch_nums = array_column($list,'touch_nums','created_hour');
        $return[] = $this->getAnalyArray('点击总次数',$total_nums);
        $return[] = $this->getAnalyArray('日活用户数',$user_nums);
        $return[] = $this->getAnalyArray('触发用户数',$touch_nums);
        //统计和
        if($params['date']){
            $count['date'] = $params['date'];
        }else{
            $count['date'] = date('Y-m-d',time());
        }
        $count['total_nums'] = array_sum($total_nums);
        $count['user_nums'] = array_sum($user_nums);
        $count['touch_nums'] = array_sum($touch_nums);

        return $this->writeJson(0,['list' =>$return,'count'=>$count]);
    }
    //模板消息回流统计
    public function tpl(){
        $request = $this->request();
        $params = $request->getRequestParam();
        if(!$params['applet']){
            return $this->writeJson(0,null,'error');
        }
        $btnModel = new CountTplsModel();
        $list = $btnModel->where(function ($query) use ($params){
            $query->where('applet',$params['applet']);
            if($params['date']){
                $query->where('created_at',$params['date']);
            }else{
                $query->where('created_at',date('Y-m-d',time()));
            }
        })->get();
        $total_nums = array_column($list,'total_nums','created_hour');
        $user_nums = array_column($list,'user_nums','created_hour');
        $touch_nums = array_column($list,'touch_nums','created_hour');
        $return[] = $this->getAnalyArray('点击总次数',$total_nums);
        $return[] = $this->getAnalyArray('日活用户数',$user_nums);
        $return[] = $this->getAnalyArray('触发用户数',$touch_nums);
        //统计和
        if($params['date']){
            $count['date'] = $params['date'];
        }else{
            $count['date'] = date('Y-m-d',time());
        }
        $count['total_nums'] = array_sum($total_nums);
        $count['user_nums'] = array_sum($user_nums);
        $count['touch_nums'] = array_sum($touch_nums);

        return $this->writeJson(0,['list' =>$return,'count'=>$count]);
    }
    //访问统计
    public function visit(){
        $request = $this->request();
        $params = $request->getRequestParam();
        if(!$params['applet']){
            return $this->writeJson(0,null,'error');
        }
        $btnModel = new CountVisitesModel();
        $list = $btnModel->where(function ($query) use ($params){
            $query->where('applet',$params['applet']);
            if($params['date']){
                $query->where('created_at',$params['date']);
            }else{
                $query->where('created_at',date('Y-m-d',time()));
            }
            if($params['page']){
                $query->where('page',$params['page']);
            }
        })->get();
        $total_nums = array_column($list,'total_nums','created_hour');
        $user_nums = array_column($list,'user_nums','created_hour');
        $touch_nums = array_column($list,'touch_nums','created_hour');
        $out_nums = array_column($list,'out_nums','created_hour');
        $return[] = $this->getAnalyArray('点击总次数',$total_nums);
        $return[] = $this->getAnalyArray('日活用户数',$user_nums);
        $return[] = $this->getAnalyArray('触发用户数',$touch_nums);
        $return[] = $this->getAnalyArray('退出次数',$out_nums);
        //统计和
        if($params['date']){
            $count['date'] = $params['date'];
        }else{
            $count['date'] = date('Y-m-d',time());
        }
        $count['total_nums'] = array_sum($total_nums);
        $count['user_nums'] = array_sum($user_nums);
        $count['touch_nums'] = array_sum($touch_nums);
        $count['out_nums'] = array_sum($out_nums);

        return $this->writeJson(0,['list' =>$return,'count'=>$count]);
    }


    /**
     * @param $name
     * @param $date
     * @param string $type
     * @param string $stack
     * @return array
     */
    public function getAnalyArray($name,$data,$type='line',$stack='总量'){
        $nowData = array();
        for($i = 0;$i<24;$i++){
            if(array_key_exists($i,$data)){
                $nowData[] = $data[$i];
            }else{
                $nowData[] = 0;
            }
        }
        return [
            'name' => $name,
            'type' => $type,
            'stack' => $stack,
            'data' => $nowData,
        ];
    }

}
