<?php

namespace App\Utility\Pool;


use EasySwoole\Component\Pool\AbstractPool;
use EasySwoole\EasySwoole\Config;
use EasySwoole\Mysqli\Config as MysqlConfig;

class MysqlPool extends AbstractPool
{
    protected function createObject()
    {
        // TODO: Implement createObject() method.
        $conf = Config::getInstance()->getConf("database");
        $dbConf = new MysqlConfig($conf);
        return new MysqlObject($dbConf);
    }
}
