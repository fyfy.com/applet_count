<?php

namespace App\Process;

use App\Utility\RedisClient;
use EasySwoole\Component\Process\AbstractProcess;

class CountInc extends AbstractProcess
{
    private $isRun = false;
    public function run($arg){
        //定时500ms检测有没有任务，有的话就while死循环执行
        $this->addTick(500,function (){
            if(!$this->isRun){
                $this->isRun = true;
                for($i=0;$i<2;$i++){
                    go(function (){
                        while (true){
                            try{
                                $redisClient = new RedisClient();
                                $task = $redisClient->onQueue('count')->read();
                                if($task){
                                    $modelName = 'App\Model\Count' . ucfirst($task['type']) . 'Model';
                                    $model = new $modelName();
                                    $redisClient->setPrefix($task['applet']);
                                    switch ($task['type']){
                                        case 'formids':
                                            //同一小程序同一天同一page同一小时触发用户数唯一(时效1小时)
                                            $hourSet = 'openids:'.$task['page'].'-'.$task['date'].'-'.$task['hour'];
                                            //同一小程序同一天只有一个openid的集合
                                            $dateSet = 'openids:all-'.$task['date'];
                                            break;
                                        case 'buttons':
                                            //同一小程序同一天同一page同一按钮同一小时触发用户数唯一(时效1小时)
                                            $hourSet = 'buttons:'.$task['page'].'-'.$task['date'].'-'.$task['hour'].'-'.$task['name'];
                                            //同一小程序同一天只有一个button的集合
                                            $dateSet = 'buttons:all-'.$task['date'];
                                            break;
                                        case 'shares':
                                            //同一小程序同一天同一小时触发用户数唯一(时效1小时)
                                            $hourSet = 'shares:hour-'.$task['date'].'-'.$task['hour'];
                                            //同一小程序同一天只有一个share的集合
                                            $dateSet = 'shares:all-'.$task['date'];
                                            break;
                                        case 'tpls':
                                            //同一小程序同一天同一小时触发用户数唯一(时效1小时)
                                            $hourSet = 'tpls:hour-'.$task['date'].'-'.$task['hour'];
                                            //同一小程序同一天只有一个tpl的集合
                                            $dateSet = 'tpls:all-'.$task['date'];
                                            break;
                                        case 'visites':
                                            //同一小程序同一天同一page同一小时触发用户数唯一(时效1小时)
                                            $hourSet = 'visites:'.$task['page'].'-'.$task['date'].'-'.$task['hour'];
                                            //同一小程序同一天只有一个visit的集合
                                            $dateSet = 'visites:all-'.$task['date'];
                                            break;
                                    }
                                    //计算要更新的字段
                                    $update = ['total_nums'];
                                    if($redisClient->sismember($hourSet,$task['openid']) == 0){
                                        //存入集合，更新触发用户数
                                        $redisClient->sAdd($hourSet,$task['openid']);
                                        if($redisClient->ttl($hourSet)<0){
                                            $redisClient->expire($hourSet,60*60);
                                        }
                                        $update[] = 'touch_nums';
                                    }
                                    if($redisClient->sismember($dateSet,$task['openid']) == 0){
                                        //存入集合，更新去重用户数(时效24小时)
                                        $redisClient->sAdd($dateSet,$task['openid']);
                                        if($redisClient->ttl($dateSet)<0){
                                            $redisClient->expire($dateSet,60*60*24);
                                        }
                                        $update[] = 'user_nums';
                                    }
                                    $model->inc($task['model_id'],$update);
                                    unset($update);
                                    unset($model);
                                }
                                unset($task);
                                unset($redisClient);
                            }catch (\Throwable $throwable){
                                throw new \Exception($throwable->getMessage());
                            }
                        }
                        $this->isRun = false;
                    });
                }
            }
        });
    }

    public function onShutDown()
    {
        // TODO: Implement onShutDown() method.
    }

    public function onReceive(string $str, ...$args)
    {
        // TODO: Implement onReceive() method.
    }
}
