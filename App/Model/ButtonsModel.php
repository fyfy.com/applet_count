<?php
/**
 * Created by PhpStorm.
 * User: Da Xiong
 * Date: 2019/3/16
 * Time: 17:45
 */

namespace App\Model;


class ButtonsModel extends BaseModel
{
    protected $table = 'buttons';

    protected $fillable = [
        'id','name','title'
    ];
}
